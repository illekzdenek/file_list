"use client";

import { FileSaveType } from "@/types/file";
import { STORAGE_FILES } from "../constants/globalConstants";
import {
  createContext,
  useContext,
  Dispatch,
  SetStateAction,
  useState,
  ReactNode,
} from "react";

const storedFiles = window.localStorage.getItem(STORAGE_FILES);

export interface ContextProps {
  files: FileSaveType[];
  setFiles: Dispatch<SetStateAction<FileSaveType[]>>;
  dateTimeFrom: string;
  setDateTimeFrom: Dispatch<SetStateAction<string>>;
  dateTimeTo: string;
  setDateTimeTo: Dispatch<SetStateAction<string>>;

}

const GlobalContext = createContext<ContextProps>({
  files: storedFiles ? JSON.parse(storedFiles) : [],
  setFiles: (): FileSaveType[] => [],
  dateTimeFrom: "",
  setDateTimeFrom: (): string => "",
  dateTimeTo: "",
  setDateTimeTo: (): string => "",
});

export const GlobalContextProvider = ({
  children,
}: {
  children: ReactNode;
}) => {
  const [files, setFiles] = useState<FileSaveType[]>(
    storedFiles ? JSON.parse(storedFiles) : []
  );
  
  const [dateTimeFrom, setDateTimeFrom] = useState<string>("");
  
  const [dateTimeTo, setDateTimeTo] = useState<string>("");

  return (
    <GlobalContext.Provider value={{ files, setFiles, dateTimeFrom, setDateTimeFrom, dateTimeTo, setDateTimeTo }}>
      {children}
    </GlobalContext.Provider>
  );
};

export const useGlobalContext = () => useContext(GlobalContext);
