import styles from "./page.module.css";
import AddNewFile from "./components/AddNewFile";
import FileTable from "./components/FileTable";

export default function Home() {
  return (
    <main className={styles.main}>
      <AddNewFile />
      <FileTable />
    </main>
  );
}
