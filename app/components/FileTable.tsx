"use client";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { getFile } from "../../utils/convertFilesHelper";
import { FileSaveType } from "@/types/file";
import { Button } from "@mui/material";
import { useGlobalContext } from "@/store/store";
import { STORAGE_FILES } from "@/constants/globalConstants";

export default function BasicTable() {
  const { files, setFiles } = useGlobalContext();

  const handleRemoveFileClick = (name: string, id: number): void => {
    let newStoredFiles = [];
    for (let storedFile of files) {
      if (storedFile.name !== name || storedFile.id !== id) {
        newStoredFiles.push(storedFile);
      }
    }
    setFiles(newStoredFiles);
    window.localStorage.setItem(STORAGE_FILES, JSON.stringify(newStoredFiles));
  };

  return (
    <>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Název</TableCell>
              <TableCell align="right">Typ</TableCell>
              <TableCell align="right">Poslední změna</TableCell>
              <TableCell align="right">Dataset od</TableCell>
              <TableCell align="right">Dataset do</TableCell>
              <TableCell align="right">Velikost (b)</TableCell>
              <TableCell align="right">Akce</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {files.map((rowFile: FileSaveType, index: number) => {
              const buildedFile = getFile(rowFile);
              console.log(buildedFile);
              const date = new Date(buildedFile.lastModified);
              const dateFrom = new Date(rowFile.dataFrom);
              const dateTo = new Date(rowFile.dataTo);
              return (
                <TableRow
                  key={buildedFile.name + buildedFile.lastModified + index}
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    {buildedFile.name}
                  </TableCell>
                  <TableCell align="right">{buildedFile.type}</TableCell>
                  <TableCell align="right">
                    {date.toLocaleDateString() +
                      " " +
                      date.toLocaleTimeString()}
                  </TableCell>

                  <TableCell align="right">
                    {dateFrom.toLocaleDateString() +
                      " " +
                      dateFrom.toLocaleTimeString()}
                  </TableCell>
                  <TableCell align="right">
                    {dateTo.toLocaleDateString() +
                      " " +
                      dateTo.toLocaleTimeString()}
                  </TableCell>

                  <TableCell align="right">{buildedFile.size}</TableCell>
                  <TableCell align="right">
                    <Button>
                      <a download={buildedFile.name} href={rowFile.base64}>
                        Stáhnout
                      </a>
                    </Button>
                    <Button
                      onClick={() =>
                        handleRemoveFileClick(rowFile.name, rowFile.id)
                      }
                    >
                      Odstranit
                    </Button>
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
}
