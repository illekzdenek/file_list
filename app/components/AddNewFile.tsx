"use client";
import React, { ChangeEvent, ReactElement } from "react";

import styles from "./add_new_file.module.css";
import { Grid, Input } from "@mui/material";
import { gotFile } from "@/utils/convertFilesHelper";
import { useGlobalContext } from "@/store/store";
import { FileSaveType } from "@/types/file";
import { STORAGE_FILES } from "@/constants/globalConstants";

type Props = {};

export default function AddNewFile({}: Props) {
  const {
    files,
    setFiles,
    dateTimeFrom,
    setDateTimeFrom,
    dateTimeTo,
    setDateTimeTo,
  } = useGlobalContext();

  const handleFromDateChange = (e: ChangeEvent | undefined): void => {
    const element = e?.target as HTMLInputElement;
    setDateTimeFrom(element.value);
  };

  const handleToDateChange = (e: ChangeEvent | undefined): void => {
    const element = e?.target as HTMLInputElement;
    setDateTimeTo(element.value);
  };

  const handleNewFileAddedChange = async (
    e: ChangeEvent | undefined
  ): Promise<void> => {
    console.log(e);
    if (e?.target !== undefined) {
      const element = e?.target as HTMLInputElement;

      const result = await gotFile(element.files);

      if (element.files !== null) {
        const newFiles = [
          ...files,
          {
            base64: result,
            name: element.files[0].name,
            type: element.files[0].type,
            lastModified: element.files[0].lastModified,
            id: files.length + 1,
            dataFrom: dateTimeFrom,
            dataTo: dateTimeTo,
          } as FileSaveType,
        ];

        setFiles(newFiles);
        window.localStorage.setItem(STORAGE_FILES, JSON.stringify(newFiles));
      }
    }
  };

  const getFileInput = (): ReactElement => {
    if (dateTimeFrom !== "" && dateTimeTo !== "") {
      return (
        <Input
          type="file"
          onChange={(e) => handleNewFileAddedChange(e)}
          className={`${styles.main}`}
        />
      );
    }
    return (
      <Input
        disabled
        type="file"
        onChange={(e) => handleNewFileAddedChange(e)}
        className={`${styles.main}`}
      />
    );
  };

  return (
    <Grid container xs={4}>
      <Grid item xs={12}>
        Dataset od:
        <Input
          size="small"
          type="datetime-local"
          onChange={handleFromDateChange}
          className={`${styles.main}`}
        />
      </Grid>
      <Grid item xs={12}>
        Dataset do:
        <Input
          size="small"
          type="datetime-local"
          onChange={handleToDateChange}
          className={`${styles.main}`}
        />
      </Grid>
      <Grid item xs={12}>
        {getFileInput()}
      </Grid>
    </Grid>
  );
}
