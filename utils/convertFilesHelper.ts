import { FileSaveType } from "@/types/file";

export async function gotFile(element: any) {
  const promise = new Promise((resolve, reject) => {
    let file = element[0];
    let reader = new FileReader();
    reader.onload = function () {
      resolve(reader.result);
    };
    reader.onerror = function () {
      reject(reader.error);
    };
    reader.readAsDataURL(file);
  });
  return promise;
}

export function getFile(fileObject: FileSaveType) {
  let base64Parts = fileObject.base64.split(",");
  let fileFormat = base64Parts[0].split(";")[0];
  let fileContent = base64Parts[1];
  let file = new File([fileContent], fileObject.name, {
    type: fileFormat,
    lastModified: fileObject.lastModified,
  });
  return file;
}
