export interface FileSaveType {
  name: string;
  type: string;
  base64: string;
  lastModified: number;
  id: number;
  dataFrom: string;
  dataTo:string;
}
